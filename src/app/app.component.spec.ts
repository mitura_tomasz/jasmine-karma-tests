import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { CounterComponent } from './components/counter/counter.component';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        CounterComponent
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  // it(`should have as title 'app'`, async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.debugElement.componentInstance;
  //   expect(app.title).toEqual('app');
  // }));
  // it('should render title in a h1 tag', async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('h1').textContent).toContain('Welcome to jasmine-karma-tests!');
  // }));
});

describe('hellotest', () => {
  let expected = '';
  let notExpected = '';
  let expectMatch = null;

  beforeEach(() => {
     expected = 'hellotest';
     notExpected = 'hellotest123';
     expectMatch = new RegExp(/^hello/);
  });

  afterEach(() => {
    let expected = '';
    let notExpected = '';
  });

  it('checks if hellotest is hellotest', () => expect('hellotest').toBe(expected));
  it('checks if hellotest is not hellotest', () => expect('hellotest').not.toBe(notExpected));
  it('checks if hellotest starts with hello', () => expect('hellotest').toMatch(expectMatch));
})
