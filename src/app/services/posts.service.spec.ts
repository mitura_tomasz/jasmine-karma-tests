import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { PostsService } from './posts.service';
import { Post } from '../models/post.model';

describe('PostsService', () => {
  let service: PostsService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PostsService]
    });

    service = TestBed.get(PostsService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', inject([PostsService], (service: PostsService) => {
    expect(service).toBeTruthy();
  }));

  it('should retrieve posts from the API via GET', () => {
    const dummyPosts: Post[] = [
      { userId: '1', id: 1, body: 'Hello World', title: 'Testing Angular' },
      { userId: '2', id: 2, body: 'Hello World', title: 'Testing Angular' }
    ];

    service.getPosts().subscribe(posts => {
      expect(posts.length).toBe(2);
      expect(posts).toEqual(dummyPosts);
    });
    
    const request = httpMock.expectOne('https://jsonplaceholder.typicode.com/posts');

    expect(request.request.method).toBe('GET');

    request.flush(dummyPosts);

  });
});
