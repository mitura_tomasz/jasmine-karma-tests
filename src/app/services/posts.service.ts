import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from '../models/post.model';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  postsUrl = 'https://jsonplaceholder.typicode.com/posts';

  constructor(private _http: HttpClient) { }

  getPosts() {
    return this._http.get<Post[]>(this.postsUrl);
  }

}
