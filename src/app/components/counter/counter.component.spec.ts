import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CounterComponent } from './counter.component';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { AppComponent } from '../../app.component';

describe('CounterComponent', () => {
  let component: CounterComponent;
  let fixture: ComponentFixture<CounterComponent>;
  let debugElement: DebugElement
  let htmlElement: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CounterComponent,
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CounterComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();

    debugElement = fixture.debugElement.query(By.css('p'));
    htmlElement = debugElement.nativeElement;
  });

  it('should display the current number of the counter', () => {
    //AAssert that the text on screen is of Number: 1
    expect(htmlElement.textContent).toEqual('Number: 1');
  });

  it('should increment the counter number by one', () => {
    // Arrange
    const initialValue = component.counter;

    // Act
    component.increment();
    fixture.detectChanges();
    const newValue = component.counter;

    // Assert
    expect(initialValue).toBeLessThan(newValue);
  });

  it('should display the counter number on screen, after being incremented by one', () => {
    component.increment();
    // fixture.detectChanges();

    expect(htmlElement.textContent).toEqual('Number: 1');
  });

  it('should decrement the counter number by one', () => {
    // Arrange
    const initialValue = component.counter;

    // Act
    component.increment();
    // fixture.detectChanges();
    const newValue = component.counter;

    // Assert
    expect(initialValue).toBeLessThan(newValue);
  });
});
